<?php
include "../config.php";

$imap = imap_open("{" . $imap_host . "/imap/ssl}INBOX", $imap_username, $imap_password);
$mailbox = imap_check($imap);
$fetch = imap_fetch_overview($imap, "1:{$mailbox->Nmsgs}");

$mysqli = mysqli_connect($sql_host, $sql_username, $sql_password, $sql_database);

foreach ($fetch as $item) { # loop through all mails

	$subject = explode(' ', imap_mime_header_decode($item->subject)[0]->text);

	if ($subject[0] == 'Internet-Adresse:') { # if subject is

		$mailfrom = imap_mime_header_decode($item->from)[0]->text;

		if ($result = mysqli_query($mysqli, "SELECT mapkey FROM mapnumeric WHERE maptable = 'fritzlog' AND mapcontent = '" . $mailfrom . "';")) { # if sender is valid

			$ipaddress = ip2long($subject[1]);
			$unixTimestamp = strtotime($item->date);
			$mysqltime = date('Y-m-d H:i:s', $unixTimestamp);
			$mapkey = mysqli_fetch_assoc($result);
			$fritzbox = $mapkey['mapkey'];

			$sql = "SELECT COUNT(id) FROM fritzlog WHERE ipaddress = '" . $ipaddress . "' AND datetime = '" . $mysqltime . "' AND fritzbox = '" . $fritzbox . "';";
			if (intval(mysqli_fetch_assoc(mysqli_query($mysqli, $sql))['COUNT(id)']) > 0) { # if data is already in the database
				imap_delete($imap, $item->uid, FT_UID);
			} else {
				$sql = "INSERT INTO fritzlog (ipaddress, datetime, fritzbox) VALUES ('" . $ipaddress . "','" . $mysqltime . "','" . $fritzbox . "');";
				mysqli_query($mysqli, $sql);
			}
		}
	}
}

imap_close($imap);
mysqli_close($mysqli);
