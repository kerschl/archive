# FritzLog2

FritzLog is a script that saves fritzbox logs from mail to a sql database

### functions

- imap_open _close - Open/close IMAP stream
- imap_check - Check current mailbox
- imap_fetch_overview - Fetches mail headers for the given sequence
- imap_mime_header_decode - Decodes strange mail strings
- imap_delete - Marks message for deletion
- imap_expunge - Delete all messages marked for deletion

### sql

```sql
CREATE TABLE fritzlog
(id int NOT NULL AUTO_INCREMENT,
	PRIMARY KEY(id),
	ipaddress int,
	datetime DATETIME,
	fritzbox int
);

Alter Table fritzlog ADD CONSTRAINT UniqueLogs UNIQUE(ipaddress, datetime, fritzbox);

CREATE TABLE mapnumeric
(id int NOT NULL AUTO_INCREMENT,
	PRIMARY KEY(id),
	maptable varchar(255),
	mapkey int,
	mapcontent varchar(255)
);

Alter Table mapnumeric ADD CONSTRAINT UniqueLogs UNIQUE(maptable, mapkey, mapcontent);
```

### notes

https://codepen.io/_Sabine/pen/XooJYy

https://codepen.io/oliviale/pen/Pygbbm

https://codepen.io/Lunnaris/pen/vKWvQN
