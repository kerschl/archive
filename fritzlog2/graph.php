<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Fritzlog</title>
	<?php include "css.php" ?>
</head>

<?php
date_default_timezone_set("Europe/Berlin");
include "../config.php";

$conn = mysqli_connect($sql_host, $sql_username, $sql_password, $sql_database);
if (!$conn) {
	die("Connection failed: " . mysqli_connect_error());
}

$sql = "SELECT mapnumeric.mapkey, mapnumeric.mapcontent FROM mapnumeric Where mapnumeric.maptable = 'fritzlog';";
$boxes = mysqli_query($conn, $sql);

$today = new DateTime();
date_time_set($today, 23, 59, 59);
$polylines = "";
$styles = "<style>";
$legend = "";
if (mysqli_num_rows($boxes) > 0) {
	while ($box = mysqli_fetch_array($boxes)) { // loop boxes
		$class = "color-" . $box["mapkey"];
		$styles = $styles . '.' . $class . ':before{background:var(--' . $class . ');}.' . $class . '{stroke:var(--' . $class . ');}';
		$legend = $legend . '<span class="' . $class . '">' . $box["mapcontent"] . '</span>';
		$p = new Polyline();
		$p->set_class($class);
		for ($i = 0; $i < 14; $i++) {
			$p->data_push(200); // init dataset
		}
		$sql = "SELECT fritzlog.datetime FROM fritzlog Where fritzlog.fritzbox = '" . $box["mapkey"] . "' AND datetime > cast((now() - interval 14 day) as date);";
		$fritzlog = mysqli_query($conn, $sql); // get data for each box
		if (mysqli_num_rows($fritzlog) > 0) {
			while ($log = mysqli_fetch_array($fritzlog)) {
				$date = date_create($log["datetime"]);
				$diff = date_diff($today, $date);
				$daysdiff = 13 - $diff->format("%a");
				$p->set_data($daysdiff, $p->get_data($daysdiff) - 20); // edit dataset
			}
		}
		$polylines = $polylines . $p->get_polyline();
	}
} else {
	die("<p>no logs found</p>");
}
$styles = $styles . "</style>";
$width = 14 * 50; // tage mal 50 px
$dates = "";
for ($i = 14; $i > 0; $i--) {
	$dates = $dates . '<span class="date">' . date("d.m.", mktime(date("H"), date("i"), date("s"), date("m"), date("d") - $i + 1)) . '</span>';
}
?>

<body>
	<?= $styles ?>
	<div class="frame" style="width: <?= $width ?>px">
		<div class="legend">
			<?= $legend ?>
		</div>
		<div class="statistic">
			<div class="data">
				<svg style="width: <?= $width ?>px; height: 210px">
					<line x1="0" y1="200" x2="<?= $width ?>" y2="200" style="stroke:lightgray;stroke-width:1;"></line>
					<line x1="0" y1="180" x2="<?= $width ?>" y2="180" style="stroke:lightgray;stroke-width:1;"></line>
					<line x1="0" y1="160" x2="<?= $width ?>" y2="160" style="stroke:lightgray;stroke-width:1;"></line>
					<line x1="0" y1="140" x2="<?= $width ?>" y2="140" style="stroke:lightgray;stroke-width:1;"></line>
					<line x1="0" y1="120" x2="<?= $width ?>" y2="120" style="stroke:lightgray;stroke-width:1;"></line>
					<line x1="0" y1="100" x2="<?= $width ?>" y2="100" style="stroke:lightgray;stroke-width:1;"></line>
					<line x1="0" y1="80" x2="<?= $width ?>" y2="80" style="stroke:lightgray;stroke-width:1;"></line>
					<line x1="0" y1="60" x2="<?= $width ?>" y2="60" style="stroke:lightgray;stroke-width:1;"></line>
					<line x1="0" y1="40" x2="<?= $width ?>" y2="40" style="stroke:lightgray;stroke-width:1;"></line>
					<line x1="0" y1="20" x2="<?= $width ?>" y2="20" style="stroke:lightgray;stroke-width:1;"></line>
					<?= $polylines ?>
				</svg>
			</div>
		</div>
		<div class="dates">
			<?= $dates ?>
		</div>
	</div>
</body>

</html>

<?php
class Polyline
{
	private $data = array();
	private $class;
	function data_push(int $val)
	{
		array_push($this->data, $val);
	}
	function set_data(int $id, int $val)
	{
		$this->data[$id] = $val;
	}
	function get_data(int $id)
	{
		return $this->data[$id];
	}
	function set_class(string $class)
	{
		$this->class = $class;
	}
	function get_polyline()
	{
		$str = '<polyline class="' . $this->class . '" points="';
		$int = 25;
		foreach ($this->data as $point) {
			$str = $str . $int . ',' . $point . ' ';
			$int = $int + 50;
		}
		return $str . '"></polyline>';
	}
}
