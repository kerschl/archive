<style>
	:root {
		--grey: #606060;
		--color-10: #547496;
		--color-11: #643a7a;
		--color-12: #D11149;
		--color-13: #1A8FE3;
		--color-14: #F17105;
		--color-15: #6610f2;
		--color-16: #E6C229;
	}
</style>
<style>
	.frame {
		margin: 0 auto;
		padding: 50px 0;
	}
</style>
<style>
	.legend {
		text-align: right;
		padding: 14px 0;
	}

	.legend>span {
		position: relative;
		font-size: 1rem;
		color: var(--grey);
		line-height: 14px;
		padding: 0 10px 0 25px;
	}

	.legend>span:before {
		position: absolute;
		left: 6px;
		top: 9px;
		display: block;
		content: "";
		width: 11px;
		height: 5px;
		border-radius: 3px;
	}
</style>
<style>
	@keyframes dash {
		to {
			stroke-dashoffset: 0;
		}
	}

	svg>polyline {
		fill: none;
		stroke-width: 6;
		stroke-linecap: round;
		stroke-dasharray: 1000;
		stroke-dashoffset: 1000;
		animation: 3s dash 0.5s linear forwards;
	}
</style>
<style>
	.dates {
		display: flex;
	}

	.date {
		width: 50px;
		font-size: 12px;
		color: var(--grey);
		line-height: 24px;
		text-transform: uppercase;
		text-align: center;
	}
</style>
