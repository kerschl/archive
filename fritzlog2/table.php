<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Fritzlog</title>
	<style>
		table {
			border-collapse: collapse;
			margin: 0 auto;
		}

		table,
		th,
		td {
			border: 1px solid;
			padding: 3px;
		}
	</style>
</head>

<?php
include "../config.php";

$conn = mysqli_connect($sql_host, $sql_username, $sql_password, $sql_database);

if (!$conn) {
	die("Connection failed: " . mysqli_connect_error());
}

$logs = mysqli_query($conn, "SELECT fritzlog.ipaddress, fritzlog.datetime, mapnumeric.mapcontent FROM fritzlog INNER JOIN mapnumeric ON fritzlog.fritzbox=mapnumeric.mapkey Where mapnumeric.maptable = 'fritzlog' ORDER BY `fritzlog`.`datetime` DESC;");

echo "<table>";

if (mysqli_num_rows($logs) > 0) { ?>
	<tr>
		<th>Date/Time</th>
		<th>Fritzbox</th>
		<th>IP Address</th>
	</tr>
	<?php
	while ($log = mysqli_fetch_array($logs)) { ?>
		<tr>
			<td>
				<?php echo $log["datetime"]; ?>
			</td>
			<td>
				<?php echo $log["mapcontent"]; ?>
			</td>
			<td>
				<?php echo long2ip($log["ipaddress"]); ?>
			</td>
		</tr>
<?php }
} else {
	echo "<p>no logs found</p>";
}

echo "</table>";

mysqli_close($conn);
?>

</html>
