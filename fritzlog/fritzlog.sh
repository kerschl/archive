# vars
urlFritzBox=http://192.168.178.1/
pwFritzBox=pw
dbuser=username
dbpw=pw
dbname=name

# start chromedriver
chromedriver & sleep 1;
# open session and save id
session=$(curl  -d '{ "desiredCapabilities": { "caps": { "nativeEvents": false, "browserName": "chrome", "version": "", "platform": "ANY" } } }'  http://localhost:9515/session | awk -F '"' '{print $4}');
echo $session'\n';

# goto router ip
curl -d '{"url":"'$urlFritzBox'"}' http://localhost:9515/session/$session/url;
sleep 3;

# select element uiPass
element=$(curl -d '{"using":"id","value":"uiPass"}' http://localhost:9515/session/$session/element | awk -F '"' '{print $12}');
echo $element'\n';
# and enter pass
curl -d '{"value":["'$pwFritzBox'"]}' http://localhost:9515/session/$session/element/$element/value;

# let page load
sleep 1;

# select element submitLoginBtn
element=$(curl -d '{"using":"id","value":"submitLoginBtn"}' http://localhost:9515/session/$session/element | awk -F '"' '{print $12}');
echo $element'\n';
# and click
curl -d '{}' http://localhost:9515/session/$session/element/$element/click;

# let page load
sleep 6;

# select element sys
element=$(curl -d '{"using":"id","value":"sys"}' http://localhost:9515/session/$session/element | awk -F '"' '{print $12}');
echo $element'\n';
# and click
curl -d '{}' http://localhost:9515/session/$session/element/$element/click;

# let page load
sleep 1;

# select element log
element=$(curl -d '{"using":"id","value":"log"}' http://localhost:9515/session/$session/element | awk -F '"' '{print $12}');
echo $element'\n';
# and click
curl -d '{}' http://localhost:9515/session/$session/element/$element/click;

# let system log load
sleep 50;

# get html
curl http://localhost:9515/session/$session/source > /tmp/fritzbox.txt

# select element blueBarUserMenuIcon
element=$(curl -d '{"using":"id","value":"blueBarUserMenuIcon"}' http://localhost:9515/session/$session/element | awk -F '"' '{print $12}');
echo $element'\n';
# and click
curl -d '{}' http://localhost:9515/session/$session/element/$element/click;

# let page load
sleep 1;

# select element logout
element=$(curl -d '{"using":"id","value":"logout"}' http://localhost:9515/session/$session/element | awk -F '"' '{print $12}');
echo $element'\n';
# and click
curl -d '{}' http://localhost:9515/session/$session/element/$element/click;

# let page load
sleep 5;

# delete session
curl -X DELETE http://localhost:9515/session/$session;

# awk replace strings to sql # pipe magic
grep -ohP 'class=\\"date\\">.*?class=\\"print\\"' /tmp/fritzbox.txt | sed 's/\./>/;s/\./>/;s/\\/>/g' | awk -F'[>]' '{printf "INSERT INTO FritzLog (date, time, msg) VALUES (\"%s-%s-%s\",\"%s\",\"%s\");\n",$6,$5,$4,$12,$18}' > /tmp/datafile.sql
#INSERT INTO FritzLog (date, time, msg) VALUES ('00-00-00', '00:00:00', 'msg');
#mariadb -u $dbuser --password=$dbpw myDB < "/tmp/datafile.sql"
cat /tmp/datafile.sql | while read line; do  mariadb -u $dbuser --password=$dbpw $dbname -e "$line"; done
#profit

