# Old Fritzlog

FritzLog was the idea to make scripts to collect an save FritzBox logs. Since FritzBoxes can now send detailed Mails this idea was not pursued further.

### > fritzlog.sh

Uses chromedriver to open the FritzBox webinterface and navigates to the log site. Then inserts the data sets, grabbed from the html dom, into a sql database.

### > requirements

- working chromedriver installed
- a sql database running somewhere
- a FritzBox you can access by url or ip

### > setup database

- `Create User '$dbuser'@'localhost' identified by '';`
- `CREATE DATABASE myDB;`
- `Grant All privileges on myDB.* to '$dbuser'@'localhost';`
- `Use myDB;`
- `CREATE TABLE myDB.FritzLog (id int NOT NULL AUTO_INCREMENT, PRIMARY KEY(id),date CHAR(8),time CHAR(8),msg VARCHAR(255));`
- `Alter Table myDB.FritzLog ADD CONSTRAINT UniqueLogs UNIQUE(date, time, msg);`
- `mariadb -u $dbuser --password=$dbpw -e "Select * From myDB.FritzLog order by date,time;"`

### > notable documentation

- https://www.pawangaria.com/post/automation/browser-automation-from-command-line/
- https://www.selenium.dev/documentation/legacy/json_wire_protocol/

